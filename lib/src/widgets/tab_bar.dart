import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class CustomTab extends StatefulWidget {
  final String label;
  final int selectedPage;
  final int pageNumber;
  final Function onPressed;
  CustomTab({this.label, this.selectedPage, this.pageNumber, this.onPressed});
  @override
  _CustomTabState createState() => _CustomTabState();
}

class _CustomTabState extends State<CustomTab> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: Container(
        child: Text(
          widget.label,
          style: widget.selectedPage == widget.pageNumber
              ? TextStyle(color: Colors.white, fontWeight: FontWeight.w600)
              : TextStyle(color: Colors.white, fontWeight: FontWeight.w100),
        ),
      ),
    );
  }
}
