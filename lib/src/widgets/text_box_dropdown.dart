import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class TextBoxDrop extends StatefulWidget {
  final String label;
  final String text;
  TextBoxDrop({this.label, this.text});
  @override
  _TextBoxDropState createState() => _TextBoxDropState();
}

class _TextBoxDropState extends State<TextBoxDrop> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 10),
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            color: Constants.kitGradients[29],
            borderRadius: BorderRadius.circular(4)),
        child: Row(
          children: [
            Container(child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.label,
                  style: TextStyle(
                    fontSize: 8,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff717171),
                  ),
                ),
                Text(widget.text,style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Color(0xff2A2C38),
                ),)
              ],
            ),),
            Spacer(),
            Icon(Icons.keyboard_arrow_down_sharp)
          ],
        )
    );
  }
}
