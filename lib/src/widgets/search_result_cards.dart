import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchCard extends StatefulWidget {
  final String image;
  final String name;
  final String cost;
  SearchCard({this.image, this.name, this.cost});
  @override
  _SearchCardState createState() => _SearchCardState();
}

class _SearchCardState extends State<SearchCard> {
  //double _currentSliderValue = 10;
  RangeValues _currentRangeValues = const RangeValues(0, 100);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 5.5),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        margin:EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        decoration: BoxDecoration(
          color: Color(0xffFCFCFC),
          borderRadius: BorderRadius.circular(7),
          boxShadow: [
            BoxShadow(
              color: Color(0xff494949),
              //blurRadius: 10.0, // soften the shadow
              //spreadRadius: 7.0, //extend the shadow
              offset: Offset(
                0.0, // Move to right 10  horizontally
                0.5, // Move to bottom 5 Vertically
              ),
            )
          ],
        ),
        child: Column(
          children: [
            Container(
                child: Row(
              children: [
                Container(
                  child: SvgPicture.asset(widget.image),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 60),
                ),
                Container(
                  child: SvgPicture.asset(widget.image),
                ),
                Spacer(flex: 1),
                // Text(widget.name,
                //     style: TextStyle(
                //         fontSize: 14,
                //         fontWeight: FontWeight.w700,
                //         color: Constants.kitGradients[27])),
                Spacer(flex: 10),
                Text(widget.cost,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                        color: Constants.kitGradients[27])),
              ],
            )),
            Divider(),
            Container(
                child: Column(
              children: [
                SizedBox(
                  child:Center(
                    child:Text("1 Stop",style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Color(0xfff2D2D2D)))
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
                SizedBox(
                  child: Row(
                    children: [
                      Text("17:10",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Color(0xfff2D2D2D))),
                      Spacer(),
                      Container(
                        width: screenWidth(context, dividedBy: 2),
                        height: 10,
                        // decoration: BoxDecoration(
                        //   border:Border(bottom: BorderSide(color:Color(0xfff868686)))
                        // ),
                        child:Row(
                          children: [
                            Container(
                              width:10,
                              decoration: BoxDecoration(
                                color: Color(0xfff868686),
                                shape:BoxShape.circle
                              ),
                            ),
                            SizedBox( width:75,child:Divider(color:Color(0xfff868686),),),
                            Container(
                              width:10,
                              decoration: BoxDecoration(
                                  color: Color(0xfff868686),
                                  shape:BoxShape.circle
                              ),
                            ),
                            SizedBox( width:75,child:Divider(color:Color(0xfff868686),),),
                            Container(
                              width:10,
                              decoration: BoxDecoration(
                                  color: Color(0xfff868686),
                                  shape:BoxShape.circle
                              ),
                            ),
                          ],
                        )

                      ),
                      Spacer(),
                      Text("22.30",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Color(0xfff2D2D2D)))
                    ],
                  ),
                ),

                SizedBox(
                    child: Row(
                  children: [
                    Text("CNN",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Color(0xfff868686))),
                    Spacer(flex: 1),
                    Text("29h 10m",
                        style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w400,
                            color: Color(0xfff868686))),
                    Spacer(flex: 1),
                    Text("CCJ",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Color(0xfff868686)))
                  ],
                ))
              ],
            ))
          ],
        ));
  }
}
