import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';


class TextBox extends StatefulWidget {
  final String label;
  final String text;
  final int pageNumber;
  TextBox({this.label, this.text, this.pageNumber});
  @override
  _TextBoxState createState() => _TextBoxState();
}

class _TextBoxState extends State<TextBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 2.5),
        height: screenHeight(context, dividedBy: 10),
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            color: Constants.kitGradients[29],
            borderRadius: BorderRadius.circular(4)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              widget.label ,
              style: TextStyle(
                fontSize: 8,
                fontWeight: FontWeight.w400,
                color: Color(0xff717171),
              ),
            ),
            Text(widget.text,style: widget.label == "RETURN" && widget.pageNumber == 0 ?  TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: Color(0xff717171),
            ):  TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: Color(0xff2A2C38),
            ),)
          ],
        ));
  }
}
