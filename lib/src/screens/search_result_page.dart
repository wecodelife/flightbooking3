import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/search_result_cards.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  List<String> data = [
    "Cheapest",
    "Stops",
    "Flight duration",
    "Airlines"
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[26],
          leading: IconButton(
            onPressed: (){
              pop(context);
            },
            icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.white,),
          ),
          title: Column(
            children: [
              Text("Kozhikode - Kannur",style:TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16)),
              SizedBox(height: screenHeight(context, dividedBy: 200),),
              Text("Wed 12 Nov,  Passenger", style:TextStyle(color: Colors.white, fontWeight: FontWeight.w100, fontSize:12))
            ],
          ),
          centerTitle: true,
        ),
        body:SingleChildScrollView(
          child:Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(horizontal:5),
           color: Color(0xffE5E5E5),
            child:Column(
              children: [
                Container(
                    //width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 12),
                    padding: EdgeInsets.symmetric(vertical:5),
                    color: Colors.white,
                    child:ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount:data.length,
                      itemBuilder: (BuildContext context, int index){
                        return Container(
                          height: screenHeight(context, dividedBy: 12),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            color:Constants.kitGradients[29],
                          ),
                          padding: EdgeInsets.symmetric(vertical:5, horizontal: 10),
                          margin: EdgeInsets.all(8),

                          child:Text(data[index], style:TextStyle(fontSize: 14, fontWeight: FontWeight.w700, color:Constants.kitGradients[27] )),
                        );
                      },
                    )
                ),
                Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 1),
                  padding: EdgeInsets.symmetric(vertical:10),
                  child:ListView.builder(
                    itemCount:10,
                    itemBuilder: (BuildContext context, int index){
                      return SearchCard(
                          image: "assets/images/qatar.svg",
                          //name:"Qatar Airways",
                          cost:"\$110"
                      );
                    },
                  )
                )
              ],
            )
          )
        )
      )
    );
  }
}
