import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/screens/one_way.dart';
import 'package:app_template/src/widgets/tab_bar.dart';
import 'package:app_template/src/screens/round_trip_tab.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedPage = 0;
  PageController _pageController;

  void _changePage(int pageNum) {
    setState(() {
      _selectedPage = pageNum;
      _pageController.animateToPage(pageNum,
          duration: Duration(milliseconds: 500),
          curve: Curves.fastLinearToSlowEaseIn);
    });
  }

  @override
  void initState() {
    _pageController = PageController();
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.blue,
        body: SingleChildScrollView(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: .7),
            color: Colors.white,
            child: Column(
              children: [
                Container(
                    width: screenWidth(context, dividedBy: 1),
                    //height: screenHeight(context, dividedBy: 20),
                    padding: EdgeInsets.symmetric(vertical: 20),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[26],
                    ),
                      child: _selectedPage == 0
                        ? SvgPicture.asset("assets/images/home_map.svg")
                        : Stack(
                        alignment: Alignment.center,
                            children: [
                              SvgPicture.asset("assets/images/home_map.svg"),
                               new Positioned(
                                   //left: screenWidth(context, dividedBy: 1),
                                   top: screenHeight(context, dividedBy: 10),
                                child:SvgPicture.asset("assets/icons/Vector_flight.svg")
                              ),
                            ],
                          ),),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Constants.kitGradients[26],
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              padding: EdgeInsets.only(bottom: 5),
                              decoration: BoxDecoration(
                                border: _selectedPage == 0
                                    ? Border(
                                        bottom: BorderSide(color: Colors.white))
                                    : Border(bottom: BorderSide.none),
                              ),
                              child: CustomTab(
                                label: "One way",
                                pageNumber: 0,
                                selectedPage: _selectedPage,
                                onPressed: () {
                                  _changePage(0);
                                },
                              ),
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 20),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 5),
                              decoration: BoxDecoration(
                                border: _selectedPage == 1
                                    ? Border(
                                        bottom: BorderSide(color: Colors.white))
                                    : Border(bottom: BorderSide.none),
                              ),
                              child: CustomTab(
                                label: "Round trip",
                                pageNumber: 1,
                                selectedPage: _selectedPage,
                                onPressed: () {
                                  _changePage(1);
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: PageView(
                          onPageChanged: (int page) {
                            setState(() {
                              _selectedPage = page;
                            });
                          },
                          controller: _pageController,
                          children: [
                            Container(
                              child: OneWayTab(),
                            ),
                            Container(
                              child: RoundTripTab(),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
