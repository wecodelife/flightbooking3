import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_template/src/widgets/textBox.dart';
import 'package:app_template/src/widgets/text_box_dropdown.dart';
import 'package:app_template/src/screens/search_result_page.dart';

class OneWayTab extends StatefulWidget {
  @override
  _OneWayTabState createState() => _OneWayTabState();
}

class _OneWayTabState extends State<OneWayTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        //height: screenHeight(context, dividedBy: 1),
        color: Colors.white,
        child: Column(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 6.5),
              //color: Colors.pink,
              decoration: BoxDecoration(
                color: Colors.white,
                // boxShadow: [
                //   BoxShadow(
                //     color: Colors.grey,
                //     blurRadius: 6.0,
                //   ),
                // ],
              ),
              child: Stack(
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    //color: Colors.yellow,
                    child: Row(
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Constants.kitGradients[26]))),
                              child: SvgPicture.asset(
                                  "assets/icons/Vector_from.svg"),
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 60),
                            ),
                            //Spacer(flex:4),
                            SizedBox(
                                child: Text("FROM",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Constants.kitGradients[28]))),
                          ],
                        ),
                        Spacer(),
                        Row(
                          children: [
                            SizedBox(
                                child: Text("TO",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Constants.kitGradients[28]))),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 60),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Constants.kitGradients[26]))),
                              child: SvgPicture.asset(
                                  "assets/icons/Vector_to.svg"),
                            ), //Spacer(flex:4),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 22),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      // color: Colors.blue,
                      child: Row(
                        children: [
                          Text("CCJ",
                              style: TextStyle(
                                  fontSize: 28,
                                  fontWeight: FontWeight.w700,
                                  color: Constants.kitGradients[27])),
                          Spacer(),
                          Text("CNN",
                              style: TextStyle(
                                  fontSize: 28,
                                  fontWeight: FontWeight.w700,
                                  color: Constants.kitGradients[27]))
                        ],
                      ),
                    ),
                  ),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 25),
                    left: screenWidth(context, dividedBy: 2.2),
                    child: Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Constants.kitGradients[27],
                      ),
                      child: Column(
                        children: [
                          SvgPicture.asset("assets/icons/Vector_top.svg"),
                          SvgPicture.asset("assets/icons/Vector_bottom.svg"),
                        ],
                      ),
                    ),
                  ),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 10),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Row(
                        children: [
                          Text(
                            "Calicut International Airport",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Constants.kitGradients[28]),
                          ),
                          Spacer(),
                          Text(
                            "Kannur International Airport",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Constants.kitGradients[28]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                children: [
                  TextBox(
                    label: "DEPARTURE",
                    text: "12 Nov 2020",
                    pageNumber: 0,
                  ),
                  Spacer(),
                  Container(
                    child:SvgPicture.asset("assets/icons/Vector_calendar.svg")
                  ),
                  Spacer(),
                  TextBox(
                    label: "RETURN",
                    text: "13 Nov 2020",
                    pageNumber: 0,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Padding(
              padding:EdgeInsets.symmetric(horizontal: 15),
              child:TextBoxDrop(
                  label:"TRAVELLER",
                  text:"2 Adult, 1 Children, 1 Infant"
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Padding(
              padding:EdgeInsets.symmetric(horizontal: 15),
              child:TextBoxDrop(
                  label:"CLASS",
                  text:"Economy",

              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Container(
              child:Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Search", style:TextStyle(fontSize: 26, fontWeight: FontWeight.w500, color: Constants.kitGradients[26])),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  GestureDetector(
                    onTap: (){
                      push(context, SearchPage());
                    },
                    child: Container(
                        width: screenWidth(context, dividedBy: 5),
                        height: screenHeight(context, dividedBy: 12),
                      color: Constants.kitGradients[26],
                      child:Icon(Icons.arrow_forward_ios, color:Colors.white)
                    ),
                  ),

                ],
              )
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
          ],
        ),
      ),
    );
  }
}

